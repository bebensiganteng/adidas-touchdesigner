HOW TO
------------------

1. Install Touchdesigner http://www.derivative.ca/088/Downloads/experimental.asp
2. Add the license key
3. Install bonjour SDK for windows https://developer.apple.com/bonjour/index.html
4. Install python 3.3 Windows https://www.python.org/ftp/python/3.3.3/python-3.3.3.amd64.msi
5. Install pybonjour-python3 https://github.com/depl0y/pybonjour-python3
	python setup.py install